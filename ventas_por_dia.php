<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sistema Repuestos</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
	<link rel="stylesheet" type="text/css" href="css/ventas.css?vknet28">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

<style>

.fade{
    opacity: 0.3;
	pointer-events : none;
	
  }
  .fade-in{
    opacity: 1; 
    pointer-events : auto ; 
  }
  .central{
			margin-top:5px;
			text-align: center;
			font-size:2em;
		}
		.cabezera-tabla th{
		background-color:rgba(58, 83, 135, 1);
		color:white;
	}		
	.timers{
		font-size:1.7em;
	}
	#salidaDetalle{
		opacity: 0;
	}

</style>
	
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title cabezera-boleta">
			<div>
				<h1><i class="fas fa-calendar"></i> Ver ventas del dia </h1>
				<p>Ver ventas del dia</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">ventas</li>
				
			</ul>
		</div>
		<?php $fecha_actual = date("d-m-Y");?>
		<div class="form-row">
		
		<div class="form-group col-md-6">
		<label>Fecha Inicio</label>
			<input type="date" class="form-control" id="fecha_inicio"  onchange=cargar_ventas_onchange() min="2013-01-01" max="2025-12-31" >
		</div>
	
		<div class="form-group col-md-6">
		<label>Fecha Término</label>
			<input type="date" class="form-control" id="fecha_termino"  onchange=cargar_ventas_onchange() min="2013-01-01" max="2025-12-31" >
		</div>
	</div>

		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
					<!-- <form method="POST" action="imprime_ventas.php" target="_blank">
					<input type="hidden" class="form-control" id="fecha_inicio_text" name="fecha_inicio_text">
		   			<input type="hidden" class="form-control" id="fecha_termino_text" name="fecha_termino_text">
		  			 <button type="submit" class="btn btn-primary"></i>Ver detalle ventas</button>
					 </form>
					 <br>
					 <form method="POST" action="imprime_nulas.php" target="_blank">
					<input type="hidden" class="form-control" id="fecha_inicio_text_nula" name="fecha_inicio_text_nula"> 
		   		<input type="hidden" class="form-control" id="fecha_termino_text_nula" name="fecha_termino_text_nula">
		  			 <button type="submit" class="btn btn-success"></i>Ver documentos nulos</button>
					 </form>-->
					 <button onclick="ventasDetalles()"  class="btn btn-success"></i>Cargar datos de la venta</button>
					 <br><br>
							<button class="btn btn-primary" id="btn-exportar" disabled onClick="ExportToExcel('xlsx')"><i class="fas fa-file-excel"></i> Exportar Tabla a Excel</button> 
						<div class="my-3 p-3 bg-white rounded box-shadow">
						<h6 class="border-bottom border-gray pb-2 mb-0 ">Ventas&nbsp; &nbsp; &nbsp;<span id="timer" class="timers">0 seg</span></h6>	
							
							<br><br>
							<div id="loading" class="central"></div>	
							<div class="my-3 p-3 bg-white rounded box-shadow">
							<div id="contenido">						
								<div id="salida">							
								
								</div>
								<div id="salidaDetalle">							
								
								</div>
								</div>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div>


	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/plugins/js-xlsx/xlsx.core.min.js"></script>
	<script type="text/javascript" src="js/plugins/FileSaver.min.js"></script> 
    <script type="text/javascript" src="js/plugins/html2canvas.min.js"></script>
    <script type="text/javascript" src="js/plugins/tableExport.min.js"></script>
	 <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
	<script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/funciones.js?vknet29"></script>
	<script type="text/javascript" src="js/ventas_por_dia.js?vknet29"></script>
	<script type="text/javascript" src="js/xlsx.full.min.js?vknet29"></script>
	<script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>	
	<?php include "./js/table.php"; ?>
	<script>
	function ExportToExcel(type, fn, dl) {
       var elt = document.getElementById('tablaProductosDetalle');
       var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
       return dl ?
         XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }):
         XLSX.writeFile(wb, fn || ('Ventas.' + (type || 'xlsx')));
			
    }
	
	</script>	

	<script>
	
	var fecha = new Date(); //Fecha actual
	var mes = fecha.getMonth()+1; //obteniendo mes
	var dia = fecha.getDate(); //obteniendo dia
	var ano = fecha.getFullYear(); //obteniendo año
	if(dia<10)
	  dia='0'+dia; //agrega cero si el menor de 10
	if(mes<10)
	  mes='0'+mes //agrega cero si el menor de 10
	let fecha_ini=document.getElementById('fecha_inicio').value=ano+"-"+mes+"-"+dia;
	let fecha_ter=document.getElementById('fecha_termino').value=ano+"-"+mes+"-"+dia;
	
   </script>




</body>

</html>
