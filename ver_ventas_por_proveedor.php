<!DOCTYPE html>
<html lang="en">

<head>
	<title>Reposición</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
	<link rel="stylesheet" type="text/css" href="css/ventas.css?vknet28">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">	

	<style>

.fade{
    opacity: 0.3;
	pointer-events : none;
	
  }
  .fade-in{
    opacity: 1; 
    pointer-events : auto ; 
  }
  .central{
			margin-top:5px;
			text-align: center;
			font-size:2em;
		}
		.cabezera-tabla th{
		background-color:rgba(58, 83, 135, 1);
		color:white;
		
	}	
	.timers{
		font-size:1.7em;
	}	

</style>
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title cabezera-boleta-proveedor">
			<div>
				<h1><i class="fas fa-truck"></i> Ver Reposición </h1>
				<p>Ver Reposición y detalles</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Reposición</li>
				
			</ul>
		</div>
		<?php $fecha_actual = date("d-m-Y");?>
		<div class="form-row">
		
				<div class="form-group col-md-6">
				<label>Fecha Inicio</label>
					<input type="date" class="form-control" id="fecha_inicio"  min="2020-01-01" max="2026-12-31" value="">
				</div>
			
				<div class="form-group col-md-6">
				<label>Fecha Término</label>
					<input type="date" class="form-control" id="fecha_termino"  min="2020-01-01" max="2026-12-31" value="">
				</div>
			</div>

		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
						<button style="margin-right: 15px" onclick="cargar_ventas_onchange()" class="btn btn-primary float-right"><i class="icon fa fa-cart-plus"></i>Consultar fechas</button>
						<div class="my-3 p-3 bg-white rounded box-shadow">

							<h6 class="border-bottom border-gray pb-2 mb-0 ">Ventas&nbsp; &nbsp; &nbsp;<span id="timer" class="timers">0 seg</span></h6>
							<br><br>
							<button class="btn btn-primary" onClick="ExportXLSX();"><i class="fas fa-file-excel"></i> Exportar Tabla a Excel</button> 
							
							<div id="loading" class="central"></div>
							<div id="contenido">									
								<div id="salida">
						
								</div>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div>


	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script type="text/javascript" src="js/plugins/js-xlsx/xlsx.core.min.js"></script>
    <script type="text/javascript" src="js/plugins/FileSaver.min.js"></script> 
    <script type="text/javascript" src="js/plugins/html2canvas.min.js"></script>
    <script type="text/javascript" src="js/plugins/tableExport.min.js"></script>
	<script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/funciones.js?vknet29"></script>
	<script type="text/javascript" src="js/ver_ventas_proveedor.js?vknet30"></script>	>
	<script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<?php include "./js/table.php"; ?>

	<script type="text/javaScript">   
		var f = new Date();      
        var sFileName = 'proveedores_producotos_stock ' + f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear()+ "/";
        function ExportXLSX(){
            $('#tablaProductos').tableExport({fileName: sFileName,
						type: 'xlsx',
						msonumberformat:'0'
					
                       });
        }
    </script>
	<script>
	
	var fecha = new Date(); //Fecha actual
	var mes = fecha.getMonth()+1; //obteniendo mes
	var dia = fecha.getDate(); //obteniendo dia
	var ano = fecha.getFullYear(); //obteniendo año
	if(dia<10)
	  dia='0'+dia; //agrega cero si el menor de 10
	if(mes<10)
	  mes='0'+mes //agrega cero si el menor de 10
	let fecha_ini=document.getElementById('fecha_inicio').value=ano+"-"+mes+"-"+dia;
	let fecha_ter=document.getElementById('fecha_termino').value=ano+"-"+mes+"-"+dia;
	console.log(fecha_ini);
   </script>

</body>

</html>
