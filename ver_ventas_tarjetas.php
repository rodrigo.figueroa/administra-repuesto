<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sistema Repuestos</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
	<link rel="stylesheet" type="text/css" href="css/ventas.css?vknet28">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
	<style>
		.fade{
    opacity: 0.3;
	pointer-events : none;
	
  }
  .fade-in{
    opacity: 1; 
    pointer-events : auto ; 
  }
  .central{
			margin-top:5px;
			text-align: center;
			font-size:2em;
		}
	</style>

	
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title cabezera-boleta-tarjetas">
			<div>
				<h1><i class="fab fa-cc-visa"></i> Ver ventas con tarjetas</h1>
				<p>Ver ventas, editar y eliminar</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">ventas</li>				
			</ul>
		</div>
		<?php $fecha_actual = date("d-m-Y");?>
		<div class="form-row">
		
		<div class="form-group col-md-6">
		<label>Fecha Inicio</label>
			<input type="date" class="form-control" id="fecha_inicio" onchange=cargar_ventas_onchange() min="2013-01-01" max="2025-12-31" value="">
		</div>
	
		<div class="form-group col-md-6">
		<label>Fecha Término</label>
			<input type="date" class="form-control" id="fecha_termino" onchange=cargar_ventas_onchange() min="2013-01-01" max="2025-12-31" value="">
		</div>
	</div>

		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
						<a style="margin-right: 15px" href="ingresar_productos_nuevos.php" class="btn btn-outline-primary float-right"><i class="icon fa fa-cart-plus"></i>Ingresar productos nuevos</a>
						<div class="my-3 p-3 bg-white rounded box-shadow">

							<h6 class="border-bottom border-gray pb-2 mb-0 ">Ventas</h6>
							<br><br>

							<div class="form-row">

							<div class="form-group col-md-6">
							<label>Filtrar Cajero</label>
							<select onchange="changueOnTurn()" class="form-control" id="selectTurno" name="turno">

							</select>
							</div>						
							</div>

							<div id="loading" class="central"></div>
							<br>
							<button class="btn btn-primary" onClick="ExportXLSX();"><i class="fas fa-file-excel"></i> Exportar Tabla a Excel</button> 
  								<div id="contenido">
								<div id="salida">
								<div class="my-3 p-3 bg-white rounded box-shadow">
								
							

								</div>
								</div>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div  id="contenido">		
<div id ="conTicket">
    <table id="tablaTicketCabezera">
    <tbody>
    <tr>
      	<td id="logoTicket"><img src="imagenes/logorefa.png"></td>
      	<tr>
        <tr>
     	 <td id="TituloTicketIngreso"></td>
     	 <tr>
        	<td id="direccionIngreso" class="direccionIngresoClass" colspan="3">Comercializadora de repuestos y accesorios de vehiculos motorizado</td>
      	</tr>
		  <tr>
        	<td  class="wassapMini" colspan="3" >Bulnes 342 - Quillota</td>
      </tr>
		</tr>
		<tr>
			<td  class="wassapMini" colspan="3" >33 2258652 | +56 9 42252955 ventas@refacsa.cl</td>
		</tr>
		<tr>
			<td id="fTicket" class="wassapMini" ></td>
		</tr>
        </tbody>
    </table>
    <table id="tablaTicket">
    <thead class="cabezera">    
	<tr>
          
		  <th id="colNombre" class="descrip">Descripcion&nbsp&nbsp&nbsp&nbsp</th>      
				<th id="colCantidad" class="cantita">Cant.&nbsp&nbsp</th>
				<th></th>
				<th id="colPrecio" class="preci">Precio</th>
		  <tr>
			<td><hr></td>
			<td><hr></td>
			<td><hr></td>      
			<td><hr></td>
		</tr>		
		</tr>
		</thead>
		<tbody id="Filaticket" class="fiTicket">		    	
		</tbody>
		
		<tr>
			<td><hr></td>
			<td><hr></td>
			<td><hr></td>      
			<td><hr></td>
		</tr>		
		
			<td id="totalCompra_precio" class="pagoC">0</td>
			<td></td>           
		</tr>

			</tr>		
		
			<td id="descuento" class="pagoC"></td>
			<td></td>           
			</tr>	

			</tr>		
		
			<td id="des_plata" class="pagoC"></td>
			<td></td>           
			</tr>
			</tr>		
		
			<td id="des_porc" class="pagoC"></td>
			<td></td>           
		   
	       
	</table>                   
<br>
                            
</div>
</div>

	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/plugins/js-xlsx/xlsx.core.min.js"></script>
    <script type="text/javascript" src="js/plugins/FileSaver.min.js"></script> 
    <script type="text/javascript" src="js/plugins/html2canvas.min.js"></script>
    <script type="text/javascript" src="js/plugins/tableExport.min.js"></script>
	<script src="js/plugins/pace.min.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/funciones.js?vknet29"></script>
	<script type="text/javascript" src="js/ver_ventas_tarjetas.js?vknet29"></script>
	<script type="text/javascript" src="js/xlsx.full.min.js?vknet29"></script>
	<script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<?php include "./js/table.php"; ?>
	<script>
		var fecha = new Date(); //Fecha actual
		var mes = fecha.getMonth()+1; //obteniendo mes
		var dia = fecha.getDate(); //obteniendo dia
		var ano = fecha.getFullYear(); //obteniendo año
		var h = fecha.getHours();	
		var m = fecha.getMinutes();	
		var s = fecha.getSeconds();	
		if(dia<10)
		dia='0'+dia; //agrega cero si el menor de 10
		if(mes<10)
		mes='0'+mes //agrega cero si el menor de 10
		let fecha_ac=document.getElementById('fecha_inicio').value=ano+"-"+mes+"-"+dia;
		let fecha_ter=document.getElementById('fecha_termino').value=ano+"-"+mes+"-"+dia;
		console.log(fecha_ac);
		document.getElementById('fTicket').innerHTML=dia+"-"+mes+"-"+ano + " " + h +":"+m+":"+s ;
	</script>

	<script type="text/javaScript">   
		var f = new Date();      
        var sFileName = 'ventas_tarjeta ' + f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear()+ "/";
        function ExportXLSX(){
            $('#tablaProductos').tableExport({fileName: sFileName,
						type: 'xlsx',
						msonumberformat:'0'
					
                       });
        }
    </script>


</body>

</html>
