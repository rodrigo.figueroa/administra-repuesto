var PROVEEDOR;

function cargarCategoriaPro() {

    //e.preventDefault();
    var sql = 'SELECT id,nombre_categoria FROM categoria ';
    $.ajax({
        type: 'POST',
        url: 'php/consulta.php',
        data: { sql: sql, tag: 'array_de_datos' },
        success: function(data) {
            var arreglo = JSON.parse(data);
            onCategory();

        },
        error: function(request, status, error) {
            alert("Error: Could not cargarProveedores");
        }
    });
}

//on select  category
let onCategory = async() => {
    //document.getElementById('loading').innerHTML=`<img src='imagenes/ajax-loader.gif'>`;
    var sql = 'SELECT id,nombre_categoria FROM categoria ORDER BY nombre_categoria ASC';

    //AJAX	
    $.ajax({
        type: 'POST',
        url: 'php/consulta.php',
        data: { sql: sql, tag: 'categoria' },
        success: function(data) {
            $('#selectCat').html(data).fadeIn();
            $('#selectCat option[value="55"]').attr("selected", true);
            changueOnCategory();

        },
        error: function(request, status, error) {
            alert('Error: Could not categoria');
        }
    })

}

let changueOnCategory = async() => {

    document.getElementById('loading').innerHTML = `<img src='imagenes/ajax-loader.gif'> Cargando productos......`;
    document.getElementById('contenido').className = `fade`;

    let id_cate = document.getElementById('selectCat').value;
    const onloar = await cargarProductos(id_cate);
    //const botones = await lenguaje();
}


//cat proverdor
let cargarCategoria = async(e) => {

    //document.getElementById('loading').innerHTML=`<img src='imagenes/ajax-loader.gif'> Cargando productos......`;
    //document.getElementById('contenido').className=`fade`;

    const evento = e.preventDefault();

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `SELECT id,nombre FROM proveedores`;

    const sql = { sql: consulta, tag: `array_datos` }



    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();

        // document.getElementById('loading').innerHTML=`<p>Guardado <img width='30px' src='https://cdn140.picsart.com/289521541024211.png?r1024x1024'></p>`;

        let arreglo = JSON.parse(data);

        let arr = new Array();

        for (var i = 0; i < arreglo.length; i++) {
            arr[arreglo[i][0].toString()] = arreglo[i][1];

        }

        PROVEEDOR = arr;
        const catepro = await cargarCategoriaPro()
            //const cargarPro = cargarProductos();

    } catch (error) {}

}


//*-cargar datos mediante async wait()
let cargarProductos = async(id_cate) => {

        var consulta;

        if (id_cate == 0) {
            consulta = `SELECT id,codigo,nombre,codigo_proveedor,costo,proveedor, ubicacion,stock_m,stock FROM productos ORDER BY codigo DESC`;
        } else {
            consulta = `SELECT id,codigo,nombre,codigo_proveedor,costo,proveedor, ubicacion,stock_m,stock FROM productos WHERE categoria=${id_cate} ORDER BY codigo`;
        }

        const baseUrl = 'php/consultaFetch.php';

        //let consulta=`SELECT id,codigo,nombre,codigo_proveedor,costo,proveedor, ubicacion,stock_m,stock FROM productos ORDER BY codigo DESC`;

        console.log(consulta);

        const sql = { sql: consulta, tag: `array_datos` }


        try {
            //*-llamar ajax al servidor mediate api fetch.
            const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
            //*-request de los datos en formato texto(viene todo el request)
            const data = await response.text();
            //*-se parsea solo la respuesta del Json enviada por el servidor.
            let array = JSON.parse(data);

            const tabla = await tablaProductos(array);
            //*-promesa de la funcion denguaje la ejecuto a la espera
            //*-de la respuesta del servidor.	
            const botones = await lenguaje();
            document.getElementById('contenido').className = `fade-in`;
            document.getElementById('loading').innerHTML = ``;
        } catch (error) {
            console.log('error en la conexion ', error);
        }

    }
    //*-productos


function tablaProductos(arreglo) {

    $("#salida").html('');


    $("#salida").append(`<table class="table" id="tablaProductos">
	<thead class="cabezeraTablaProductos">
	<tr>
	<th width="5%">Cód. Int</th>
	<th width="10%">Cód Pro</th>		
	<th width="20%">Nombre</th>	
	<th width="10%">Costo</th>
	<th width="10%">25%</th>
	<th width="20%">Proveedor</th>																					
	<th width="5%">Stock</th>							
	<th width="5%"> </th>
	<th width="5%"> </th>
	</tr>
	</thead>
	<tbody id="tablaBody"></tbody></table>`);

    for (var i = 0; i < arreglo.length; i++) {
        document.getElementById('cantidad_producto').innerHTML = ` - ${i+1}`;
        var id = arreglo[i]['id'];
        var codigo = arreglo[i]['codigo'];
        var codigo_proveedor = arreglo[i]['codigo_proveedor'];
        var nombre = arreglo[i]['nombre'];
        var costo = arreglo[i]['costo'];
        let toFinal = costo * 2.05 * 1.19;
        let caldes = (toFinal * 0.25);
        let desFinal = parseInt(toFinal) - parseInt(caldes);

        //console.log('desFinal ' + desFinal);

        var proveedor = arreglo[i]['proveedor'];
        var stock_m = arreglo[i]['stock_m'];
        var stock = arreglo[i]["stock"];
        $("#tablaBody").append('<tr id="' + 'fila_add' + parseFloat(i + 1) + '">' +
            '<td>' + codigo + '</td>' +
            '<td>' + codigo_proveedor + '</td>' +
            '<td>' + nombre + '</td>' +
            '<td>' + formatearNumeros(costo) + '</td>' +
            '<td>' + formatearNumeros(desFinal) + '</td>' +
            '<td>' + PROVEEDOR[proveedor] + '</td>' +
            '<td>' + stock + '</td>' +
            '<td><form method="POST" action="editar_productos.php" target="_blank">' +
            '<button id="btn_editar' + id + '" type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Editar" name="id" value="' + id + '" ><i class="fas fa-pen" aria-hidden="true"></i></button></form></td>' +
            '<td ><button id="btn_eliminar' + id + '" class="btn  btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,' + id + ',' + parseFloat(i + 1) + ')><i class="fas fa-trash-alt" aria-hidden="true"></i></button></td>' +
            '</tr>');

        // if(stock<1){
        // 	document.getElementById(`fila_add${parseFloat(i + 1)}`).className=`color_fila_rojo`;
        // }else{
        // 	document.getElementById(`fila_add${parseFloat(i + 1)}`).className=`color_fila_verde`;
        // }
    }

    $('[data-toggle="tooltip"]').tooltip();


}

function lenguaje() {

    var f = new Date();
    var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

    var table = $('#tablaProductos').DataTable({

        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "pageLength": 50,
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },


        "order": [
            [1, "desc"]
        ],
        "stateSave": true,
        "createdRow": function(row, data, index) {
            if (data[6] < 1) {
                $('td', row).css({
                    'background-color': '#F47A7A',
                    'color': 'white',


                })
            }
        },

        "lengthMenu": [100, 125, 150, 175, 1000]
    });


    new $.fn.dataTable.Buttons(table, {
        buttons: [{
            extend: 'excelHtml5',
            title: 'productos-' + fecha + ''
        }, {
            extend: 'pdfHtml5',
            title: 'productos-' + fecha + ''
        }]

    });

    table.buttons(0, null).container().prependTo(
        table.table().container()
    );


}


function eliminarProducto(e, id, idFila) {
    e.preventDefault();
    swal({
            title: "Eliminar producto",
            text: "¿esta seguro de eliminar el producto ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                borrar(id, idFila);
            } else {
                return;
            }

        });

}

let borrar = async(idP, idFila) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `DELETE FROM PRODUCTOS  WHERE id=${idP}`;

    const sql = { sql: consulta, tag: `crud` }

    console.error(consulta);

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();

        document.getElementById(`btn_eliminar${idP}`).disabled = `true`;
        document.getElementById(`btn_editar${idP}`).disabled = `true`;
        document.getElementById(`fila_add${idFila}`).className = `disabledrow`;


    } catch (error) { console.log('error en la conexion ', error); }

}

const actualizarMargen = async(e) => {

    const evento = e.preventDefault();
    let categia = document.getElementById(`selectCat`).value;
    let margen = document.getElementById(`margen`).value;

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `UPDATE PRODUCTOS set margen_contado=${margen}  WHERE categoria=${categia}`;

    const sql = { sql: consulta, tag: `crud` }

    console.error(consulta);

    try {

        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        const data = await response.text();
        swal("Margen actualizado", "el margen de los productos fue actualizado", "success");

    } catch (error) { console.log('error en la conexion ', error); }

}


window.onload = cargarCategoria