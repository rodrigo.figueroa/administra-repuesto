var VENDEDORES;
var CLIENTES;
var ESTADOVENTA = 1;


let vendedor = async() => {

    //document.getElementById('loading').innerHTML = `<i class="fas fa-spinner fa-pulse"></i> Cargando ventas espere......`;

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `	SELECT id_vendedor,nombreVendedor FROM vendedores`;


    const sql = { sql: consulta, tag: `array_datos` }
    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let array = JSON.parse(data);

        var arr = new Array();
        for (var i = 0; i < array.length; i++) {
            arr[array[i][0].toString()] = array[i][1];

        }
        VENDEDORES = arr;

        const clie = await clientes();
        const vend = await vendedorSelect();
    } catch (error) {
        console.log('error en la conexion ', error);
    }
}

//select para filtro de vendedor
let vendedorSelect = async(fecha_actual, fecha_termino) => {

    //document.getElementById('loading').innerHTML=`<i class="fas fa-spinner fa-pulse fa-2x"></i> Cargando ventas, espere .....`;
    //document.getElementById('contenido').className=`fade`;

    var sql = 'SELECT id_vendedor,nombreVendedor FROM vendedores  ORDER BY id_vendedor';



    $.ajax({
        type: 'POST',
        url: 'php/consulta.php',
        data: { sql: sql, tag: 'vendedorAsignado' },
        success: function(data) {
            $('#selectTurno').html(data).fadeIn();
            //  $('#selectCat option[value="2"]').attr("selected", true);		
            //consultarVendedor(fecha_actual,fecha_termino)

        },
        error: function(request, status, error) {
            alert('Error: Could not categoria');
        }
    })

}



let cargar_ventas_onchange = async() => {

    document.getElementById('loading').innerHTML = `<strong><i class="fas fa-sync fa-spin fa-3x"></i>&nbsp;&nbsp;</strong>`;
    document.getElementById('contenido').className = `fade`;


    let selectVen = document.getElementById('selectTurno').value;
    var consulta;
    var filtroVendedor = 'AND id_vendedor=' + selectVen;
    let fecha_inicio = document.getElementById('fecha_inicio').value;
    let fecha_termino = document.getElementById('fecha_termino').value;

    const baseUrl = 'php/consultaFetch.php';

    if (selectVen < 1) {

        consulta = `SELECT id,id_boleta,id_vendedor,id_cliente,estado_venta,(fecha_venta) as fecha,neto,iva, total,nula_boleta ,descuento,descuento_pesos,total_sin_des
					FROM ventas WHERE fecha_venta between "${fecha_inicio} 00:00:00" AND "${fecha_termino} 23:59:59" AND estado_venta=1 OR estado_venta=6`;

    } else {

        consulta = `SELECT id,id_boleta,id_vendedor,id_cliente,estado_venta,(fecha_venta) as fecha,neto,iva, total,nula_boleta ,descuento,descuento_pesos,total_sin_des
		FROM ventas WHERE fecha_venta between "${fecha_inicio} 00:00:00" AND "${fecha_termino} 23:59:59" ${filtroVendedor} AND estado_venta=1 OR estado_venta=6`;
    }


    const sql = { sql: consulta, tag: `array_datos` }



    try {

        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });

        const data = await response.text();

        let array = JSON.parse(data);

        const tablaFactutass = await tablaVentas(array);
        const le = await lenguaje();

        document.getElementById('loading').innerHTML = ``;
        document.getElementById('contenido').className = `fade-in`;

    } catch (error) {
        console.log('error en la conexion ', error);
    }


}


let changueOnTurn = () => {
    $("#tablaBody").html("").fadeIn('slow');
    document.getElementById(`totalVentaColsCaja`).innerHTML = '0';
    document.getElementById(`totalVentaColsDesc`).innerHTML = '0';
    document.getElementById(`totalVentaCols`).innerHTML = '0';

    ACTIVO = false;
    if (ACTIVO == true) {
        lenguaje();
        return;
    }
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();
    cargar_ventas_onchange();
}




let clientes = async() => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `	SELECT id,nombre FROM clientes`;


    const sql = { sql: consulta, tag: `array_datos` }
    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let array = JSON.parse(data);

        var arrs = new Array();
        for (var i = 0; i < array.length; i++) {
            arrs[array[i][0].toString()] = array[i][1];

        }
        CLIENTES = arrs;

        const vende = await cargar_ventas_onchange();
        //*-promesa de la funcion denguaje la ejecuto a la espera
        //*-de la respuesta del servidor.	


    } catch (error) {
        console.log('error en la conexion ', error);
    }
}


//*-cargar datos mediante async wait()
let cargarVentas = async() => {



    document.getElementById('contenido').className = `fade`;
    const baseUrl = 'php/consultaFetch.php';
    let consulta = `SELECT id,id_boleta,id_vendedor,id_cliente,estado_venta,DATE(fecha_venta) as fecha,neto,iva, total 
    FROM ventas`;


    const sql = { sql: consulta, tag: `array_datos` }

    try {

        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });

        const data = await response.text();

        let array = JSON.parse(data);
        const cargar = await cargar_ventas_onchange();


        document.getElementById('loading').innerHTML = ``;
    } catch (error) {
        console.log('error en la conexion ', error);
    }

}




const tablaVentas = async(arreglo) => {

    $("#salida").html('');

    $('#salida').append(`<table class="table table-striped " id="tablaProductos">
	<thead class="cabezera">
	<tr>
	<th width="5%">Boleta</th>
	<th width="10%">Fecha</th>
	<th width="10%">Vendedor</th>
	<th width="10%">Estado</th>	
	<th width="10%">Valor desc</th>
	<th width="10%">Desc dinero</th>										
	<th width="5%">%</th>
	<th width="10%">Total Sin des</th>							
	<th width="5%"> </th>
	<th width="5%"> </th>
	<th width="5%"> </th>
	<th width="5%"> </th>								
	</tr>
	</thead>
	<tbody id="tablaBody"></tbody>
	<tfoot>								
	<tr>
		<th id="totalVentaCols" style="text-align:left">Total:</th>
		<th id="totalVentaColsDesc" style="text-align:left">Total Descuento:</th>
		<th id="totalVentaColsCaja" style="text-align:left">Total Caja:</th>
		<th id="comisionCols" style="text-align:left">Comisión:</th>
		<th></th>
		<th></th>
		<th></th>
		
	</tr>
	</tfoot>
	</table>`);

    for (var i = 0; i < arreglo.length; i++) {

        let estadoColumna;
        let activo;
        let descuento;
        let desc_plata;
        let total;
        let boton;
        let boton_eliminar;
        if (arreglo[i]['nula_boleta'] == 2) {
            activo = `<span class='badge badge-dark'>Nula</span>`;
            boton = ``;
            boton_eliminar = `<button class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="top" title="Eliminar" onclick=eliminarProducto(event,${arreglo[i]['id']},1)><i class="fas fa-trash"></i></button>`
        } else {
            activo = `<span class='badge badge-success'>Realizada</span>`;
            boton = `<button class="btn  btn-dark btn-sm" data-toggle="tooltip" data-placement="top" title="Anular" onclick=eliminarProducto(event,${arreglo[i]['id']})><i class="fas fa-ban"></i></button>`;
            boton_eliminar = `<button class="btn  btn-danger btn-sm"  data-toggle="tooltip" data-placement="top" title="Eliminar" onclick=eliminarProducto(event,${arreglo[i]['id']},1)><i class="fas fa-trash-alt"></i></button>`
        }

        if (arreglo[i]['total'] == arreglo[i]['total_sin_des']) {

            descuento = 0;
            desc_plata = 0;
            total = arreglo[i]['total_sin_des'];


        } else {
            descuento = (arreglo[i]['total']);
            desc_plata = arreglo[i]['total_sin_des'] - arreglo[i]['total'];
            console.log(desc_plata);
            total = 0;
            document.getElementById("descuento").style.opacity = 1;
            document.getElementById("des_plata").style.opacity = 1;
        }


        $("#tablaBody").append(`<tr>
		<td>${arreglo[i]['id_boleta']}</td>
		<td>${arreglo[i]['fecha']}</td>				   
		<td>${VENDEDORES[arreglo[i]['id_vendedor']]}</td>
		<td>${activo}</td>
		<td>${formatearNumeros(descuento)}</td>
		<td>${formatearNumeros(desc_plata)}</td>
		<td>${(arreglo[i]['descuento'])}</td>			
		<td>${formatearNumeros(total)}</td>				  
		<td><form method="POST" action="detalle_venta.php">
		<input type="hidden" class="form-control" id="estado_venta" name="estado_venta" value="${arreglo[i]['estado_venta']}">
		<input type="hidden" class="form-control" id="num_boleta" name="num_boleta" value="${arreglo[i]['id_boleta']}">
		<button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
		data-placement="top" title="Editar" name="id" value=${arreglo[i]['id']}><i class="fas fa-pen" aria-hidden="true"></i></button></form></td>		
		<td>${boton_eliminar}</td>
		<td><button class="btn  btn-success btn-sm"  data-toggle="tooltip" data-placement="top" title="Imprimir ticket" onclick="dataTicket('conTicket',${arreglo[i]['id']},${descuento},${desc_plata},${(arreglo[i]['descuento'])},${total})"><i class="fas fa-print fa-1x" aria-hidden="true"></i></button></td>			
		<td>${boton}</td>				
	 </tr>`);
    }

    $('[data-toggle="tooltip"]').tooltip();
    totalVentasCols();



}


//llenar datos para ticket
let dataTicket = async(nombreDiv, id, descuento, desc_plata, despor, total) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `SELECT vr.id,vr.codigo_producto,p.codigo_proveedor,id_cliente,p.precio_venta,id_proveedor,p.id AS idproducto,
	vr.nombre_producto AS nombre,DATE(v.fecha_venta) AS fecha_venta, vr.cantidad,vr.precio_unitario,vr.total_unitario,vr.id_venta,vr.descuento_producto
	FROM ventas_relacional vr INNER JOIN ventas v ON v.id=vr.id_venta JOIN productos p ON p.codigo=vr.codigo_producto WHERE vr.id_venta=${id} AND v.estado_venta=1`;

    const sql = { sql: consulta, tag: `array_datos` }



    try {

        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        const data = await response.text();
        let array = JSON.parse(data);
        console.log(array);

        if (SUCURSAL == 1) {
            var sucur = "Quillota";
        } else if (SUCURSAL == 2) {
            var sucur = "La Caleras";
        } else if (SUCURSAL == 3) {
            var sucur = "Limache";
        }


        for (var i = 0; i < array.length; i++) {
            let nombreCorto;
            let nombre = array[i]['nombre'];
            let nunChar = nombre.length;
            console.log(nombre.length);

            if (nombre.length > 35) {

                nombreCorto = nombre.substring(0, 30);

            } else {
                nombreCorto = nombre;
            }


            $("#Filaticket").append('<tr id="fila' + (i + 1) + '">' +
                '<td id="colsNombre' + (i + 1) + '" class="colTicketnomVentas">' + nombreCorto + '</td>' +
                '<td id="colsCantidad' + (i + 1) + '" class="colcantidadVentas"  &nbsp&nbsp>' + array[i]['cantidad'] + '</td>' +
                '<td></td>' +
                '<td id="colsPrecio' + (i + 1) + '" class="colprecioVentas" >$' + formatearNumeros(array[i]['total_unitario']) + '</td>' +
                '</tr>');

        }

        let totalFinal;
        console.log('total ' + total);
        if (total == 0) {
            totalFinal = parseInt(desc_plata) + parseInt(descuento);

            document.getElementById("descuento").style.opacity = 1;
            document.getElementById("des_plata").style.opacity = 1;

        } else {
            totalFinal = total;
            console.log('totalFinalSuma' + totalFinal)
            document.getElementById("descuento").style.opacity = 0;
            document.getElementById("des_plata").style.opacity = 0;
        }



        document.getElementById('totalCompra_precio').innerHTML = `Total :    $${formatearNumeros(totalFinal)}`;
        document.getElementById('descuento').innerHTML = `Total Descuento :    $${formatearNumeros(desc_plata)}`;
        document.getElementById('des_plata').innerHTML = `Total Final :    $${formatearNumeros(descuento)}`;
        //document.getElementById('des_porc').innerHTML=`Descuento % :    $${despor}` ;

        let tablaC = document.getElementById("Filaticket"),
            rIndex;
        let nFilas = $("#Filaticket > tr").length;



        imprimir(nombreDiv);
        setTimeout('window.location.href = "ver_ventas.php";', 500);

    } catch (error) { console.log('error en la conexion ', error); }


}

let totalVentasCols = async() => {

    let nFilas = $("#tablaBody > tr").length;
    let tablaC = document.getElementById("tablaBody"),
        rIndex;
    let columna = 7;
    let valorTotal = 0;
    let valor = 0;
    for (let i = 0; i < nFilas; i++) {
        //valorTotal +=  parseInt(convertirNumeros(document.getElementById('prect'+(i+1)).value));
        //console.log("valor total: " + valorTotal);
        valor += parseInt(convertirNumeros(tablaC.rows[i].cells[columna].innerHTML));
    }
    document.getElementById('totalVentaCols').innerHTML = `<h6>TOTAL: $${formatearNumeros(valor)}</h6>`;
    const total = await totalVentasColsDesc(valor);

}

let totalVentasColsDesc = (valorTotal) => {


    let nFilas = $("#tablaBody > tr").length;
    let tablaC = document.getElementById("tablaBody"),
        rIndex;
    let columna = 4;
    //let valorTotal=0;
    let valor = 0;
    var toFinal;
    var totalFinaldef;
    for (let i = 0; i < nFilas; i++) {
        //valorTotal +=  parseInt(convertirNumeros(document.getElementById('prect'+(i+1)).value));
        //console.log("valor total: " + valorTotal);
        valor += parseInt(convertirNumeros(tablaC.rows[i].cells[columna].innerHTML));

    }

    toFinal = formatearNumeros(parseInt(valorTotal) + parseInt(valor));


    let comision = toFinal;
    let perComision = convertirNumeros(toFinal) * (0.02);
    console.log(redondeo(perComision, 0))

    console.log(perComision);
    document.getElementById('totalVentaColsDesc').innerHTML = `<h6>Total Descuento: $${formatearNumeros(valor)}</h6>`;
    document.getElementById('totalVentaColsCaja').innerHTML = `<h6>Caja: $${(toFinal)}</h6>`;
    document.getElementById('comisionCols').innerHTML = `<h6>Comisiòn 2%: $${formatearNumeros(redondeo(perComision,0))}</h6>`;


}


function lenguaje() {


    var f = new Date();
    var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

    var table = $('#tablaProductos').DataTable({

        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "order": [
            [1, "asc"]
        ],
        "stateSave": true,
        "lengthMenu": [50, 100, 150, 175, 1000]
    });


    new $.fn.dataTable.Buttons(table, {
        buttons: [{
                extend: 'excelHtml5',
                footer: true,
                text: '<i class="fas fa-file-excel fa-2x"></i>',
                titleAttr: 'Exportar a excel',
                className: 'btn btn-success',
                title: 'Ventas ' + fecha + ''
            }, {
                extend: 'pdfHtml5',
                footer: true,
                text: '<i class="fas fa-file-pdf fa-2x"></i>',
                titleAttr: 'Exportar a Pdf',
                className: 'btn btn-danger',
                title: 'Ventas ' + fecha + ''
            },
            {
                extend: 'print',
                footer: true,
                text: '<i class="fas fa-print fa-2x"></i>',
                titleAttr: 'Imprimir',
                className: 'btn btn-info',
                title: 'Ventas ' + fecha + ''

            }
        ]

    });

    table.buttons(0, null).container().prependTo(
        table.table().container()
    );


}


function eliminarProducto(e, id, index) {
    e.preventDefault();
    let mensaje;
    let titulo;
    if (index == 1) {
        titulo = `Eliminar dato`;
        mensaje = `¿esta seguro de eliminar la boleta ?`;
    } else {
        titulo = `Anular documento`;
        mensaje = `¿esta seguro de anular la boleta ?`;
    }

    swal({
            title: `${titulo}`,
            text: `${mensaje}`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                //borrarVenta(id);
                obtenerStock(id, index);
            } else {
                return;
            }

        });

}

let obtenerStock = async(idP, index) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `SELECT vr.id,vr.codigo_producto,p.id as idProducto,p.codigo_proveedor,id_cliente,p.precio_venta,vr.nombre_producto AS nombre,DATE(v.fecha_venta) AS fecha_venta, vr.cantidad,vr.precio_unitario,vr.total_unitario,vr.id_venta
	FROM ventas_relacional vr INNER JOIN ventas v ON v.id=vr.id_venta JOIN productos p ON p.codigo=vr.codigo_producto WHERE vr.id_venta=${idP} AND v.estado_venta=1`;

    const sql = { sql: consulta, tag: `array_datos` }

    console.error(consulta);

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.	

        let array = JSON.parse(data);


        for (let i = 0; i < array.length; i++) {

            const devol = await devolverStock(array[i]['cantidad'], array[i]['idProducto']);

        }
        if (index == 1) {
            const eliminar = await elimiarVenta(idP);
        } else {
            const borrar = await actualizarVenta(idP);
        }
        const totVanCols = await totalVentasCols();



    } catch (error) { console.log('error en la conexion ', error); }

}


let devolverStock = async(cantidad, idProducto) => {

    const baseUrl = 'php/consultaFetch.php';

    const consulta = `UPDATE productos set stock =stock + (${cantidad}) WHERE id=${idProducto}`;

    const sql = { sql: consulta, tag: `array_datos` }

    console.error(consulta);

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        console.error('actulizado');


    } catch (error) { console.log('error en la conexion ', error); }


}
let elimiarVenta = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `DELETE FROM ventas WHERE id=${idP}`;

    const sql = { sql: consulta, tag: `crud` }

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        const borraVrelacional = borrVentaRe(idP);
    } catch (error) { console.log('error en la conexion ', error); }

}

let actualizarVenta = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `UPDATE ventas set nula_boleta=2 WHERE id=${idP}`;

    const sql = { sql: consulta, tag: `crud` }

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();

        $.notify({
            title: "Anulado: ",
            message: "Se Anulo la boleta:",
            icon: 'fas fa-check'
        }, {
            type: "success",
            placement: {
                from: "top",
                align: "right"
            },
            offset: 70,
            spacing: 70,
            z_index: 1031,
            delay: 2000,
            timer: 3000
        });

        setTimeout('window.location.href = "ventas_por_dia.php"', 1000);
    } catch (error) { console.log('error en la conexion ', error); }

}

let borrVentaRe = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `DELETE FROM VENTAS_RELACIONAL  WHERE id_venta=${idP}`;

    const sql = { sql: consulta, tag: `crud` }

    console.error(consulta);

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.	


        $.notify({
            title: "Borrado: ",
            message: "Se Borro la venta:",
            icon: 'fas fa-check'
        }, {
            type: "success",
            placement: {
                from: "top",
                align: "right"
            },
            offset: 70,
            spacing: 70,
            z_index: 1031,
            delay: 2000,
            timer: 3000
        });

        setTimeout('location.reload()', 1000);


    } catch (error) { console.log('error en la conexion ', error); }


}

function imprimir(nombreDiv) {

    var contenido = document.getElementById(nombreDiv).innerHTML;
    var contenidoOriginal = document.body.innerHTML;
    document.body.innerHTML = contenido;
    window.print();
    document.body.innerHTML = contenidoOriginal;

}

const ocultarTicket = async() => {

    let ticket = document.getElementById('conTicket').style.opacity = 0;
    const ven = await vendedor();
}


window.onload = ocultarTicket