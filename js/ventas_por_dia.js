var VENDEDORES;
var CLIENTES;



let vendedor = async() => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `	SELECT id_vendedor,nombreVendedor FROM vendedores`;


    const sql = { sql: consulta, tag: `array_datos` }
    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let array = JSON.parse(data);

        var arr = new Array();
        for (var i = 0; i < array.length; i++) {
            arr[array[i][0].toString()] = array[i][1];

        }
        VENDEDORES = arr;

        const clie = await clientes();
        // let fecha_inicio = document.getElementById('fecha_inicio').value;
        // document.getElementById('fecha_inicio_text').value = fecha_inicio;
        // document.getElementById('fecha_inicio_text_nula').value = fecha_inicio;
        // let fecha_termino = document.getElementById('fecha_termino').value;
        // document.getElementById('fecha_termino_text').value = fecha_termino;
        // document.getElementById('fecha_termino_text_nula').value = fecha_termino;
        //*-promesa de la funcion denguaje la ejecuto a la espera
        //*-de la respuesta del servidor.	


    } catch (error) {
        console.log('error en la conexion ', error);
    }
}

let cargar_ventas_onchange = async() => {


    document.getElementById('loading').innerHTML = `<strong><i class="fas fa-sync fa-spin fa-3x"></i></strong>`;
    document.getElementById('contenido').className = `fade`;
    document.getElementById(`btn-exportar`).disabled = true;
    $("#salida").html('');
    $("#salida").append(`<table class="table table-striped " id="tablaProductos">
    <thead class="cabezera-tabla">
    <tr>
    <th width="10%">Fecha vta.</th>
    <th width="15%">Documento</th>
    <th width="15%">Nro</th>
    <th width="15%">Vendedor</th>
    <th width="10%">Cliente</th>	
    <th width="10%">Neto</th>
    <th width="10%">Iva</th>										
    <th width="5%">Total</th>					
    
    </tr>
    </thead>
    <tbody id="tablaBody"></tbody>
    <tfoot>								
    <tr>
        <th id="totalVentaCols" style="text-align:left">Total:</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        
    </tr>
    </tfoot>
    </table>`);


    let fecha_inicio = document.getElementById('fecha_inicio').value;
    let fecha_termino = document.getElementById('fecha_termino').value;

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `SELECT id,estado_venta,fecha_venta,id_vendedor,id_cliente,estado_venta,DATE(fecha_venta) as fecha,neto,iva, total,id_boleta,nula_boleta,nula_factura,nula_guia,nula_tarjeta,
				id_cotizacion,id_factura,id_guia,id_tarjeta FROM
				ventas WHERE fecha_venta between "${fecha_inicio} 00:00:00" AND "${fecha_termino} 23:59:59" ORDER BY estado_venta,id_boleta,id_factura,id_guia,id_tarjeta ASC`;



    const sql = { sql: consulta, tag: `array_datos` }

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.

        let array = JSON.parse(data);

        const tablaFactutass = await tablaVentas(array);

        document.getElementById('loading').innerHTML = ``;
        document.getElementById('contenido').className = `fade-in`;

    } catch (error) {
        console.log('error en la conexion ', error);
    }


}



let clientes = async() => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `	SELECT id,nombre FROM clientes`;


    const sql = { sql: consulta, tag: `array_datos` }
    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let array = JSON.parse(data);

        var arrs = new Array();
        for (var i = 0; i < array.length; i++) {
            arrs[array[i][0].toString()] = array[i][1];

        }
        CLIENTES = arrs;
        // const tabla = await lenguaje();
        //const vende = await cargarVentas();
        const cargar = await cargar_ventas_onchange();
        //*-promesa de la funcion denguaje la ejecuto a la espera
        //*-de la respuesta del servidor.	


    } catch (error) {
        console.log('error en la conexion ', error);
    }
}


//*-cargar datos mediante async wait()
let cargarVentas = async() => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `SELECT id,id_boleta,id_vendedor,id_cliente,estado_venta,DATE_FORMAT(fecha_venta ,'%m-%d-%YYYY') as fecha,neto,iva, total 
    FROM ventas ORDER BY fecha_venta DESC`;


    const sql = { sql: consulta, tag: `array_datos` }

    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let array = JSON.parse(data);
        const cargar = await cargar_ventas_onchange();
        //const tablaFactutass = await tablaVentas(array);
        //*-promesa de la funcion denguaje la ejecuto a la espera
        //*-de la respuesta del servidor.	
        const botones = await lenguaje();

    } catch (error) {
        console.log('error en la conexion ', error);
    }

}

let tablaVentas = (arreglo) => {


    let tbody = document.getElementById('tablaBody');

    for (var i = 0; i < arreglo.length; i++) {

        let estadoColumna;
        let estadoDocumento;
        let numero;


        if (CLIENTES[arreglo[i]['id_cliente']] != undefined) {
            estadoColumna = CLIENTES[arreglo[i]['id_cliente']];
        }
        if (CLIENTES[arreglo[i]['id_cliente']] == undefined) {
            estadoColumna = `<span class='badge badge-danger'>Sin cliente</span>`;
        }

        if (arreglo[i]['estado_venta'] == 1) {

            if (arreglo[i]['nula_boleta'] == 2) {

                estadoDocumento = `<span class='badge badge-dark'>nula</span>`;
            } else {
                estadoDocumento = `<span class='badge badge-success'>Boleta</span>`;
            }
            numero = arreglo[i]['id_boleta'];
        } else if (arreglo[i]['estado_venta'] == 2) {

            if (arreglo[i]['nula_factura'] == 2) {
                estadoDocumento = `<span class='badge badge-dark'>nula</span>`;
            } else {
                estadoDocumento = `<span class='badge badge-warning'>Factura</span>`;
            }

            numero = arreglo[i]['id_factura'];
        } else if (arreglo[i]['estado_venta'] == 3) {

            if (arreglo[i]['nula_guia'] == 2) {
                estadoDocumento = `<span class='badge badge-dark'>nula</span>`;
            } else {
                estadoDocumento = `<span class='badge badge-danger'>Guía</span>`;
            }

            numero = arreglo[i]['id_guia'];
        } else if (arreglo[i]['estado_venta'] == 4) {

            estadoDocumento = `<span class='badge badge-dark'>Cotización</span>`;
            numero = arreglo[i]['id_cotizacion'];
        } else if (arreglo[i]['estado_venta'] == 5) {

            if (arreglo[i]['nula_tarjeta'] == 2) {
                estadoDocumento = `<span class='badge badge-dark'>nula</span>`;
            } else {
                estadoDocumento = `<span class='badge badge-primary'>Tarjeta</span>`;
            }

            numero = arreglo[i]['id_tarjeta'];
        }

        if (arreglo[i]['estado_venta'] != 4) {

            $("#tablaBody").append(`<tr>
							<td>${arreglo[i]['fecha_venta']}</td>
							<td>${estadoDocumento}</td>	
							<td>${numero}</td>			   
							<td>${VENDEDORES[arreglo[i]['id_vendedor']]}</td>
							<td>${estadoColumna}</td>
						   <td>${formatearNumeros(arreglo[i]['neto'])}</td>
						   <td>${formatearNumeros(arreglo[i]['iva'])}</td>					
						   <td>${formatearNumeros(arreglo[i]['total'])}</td>	 
						 </tr>`);
        } else {

            console.error('4');
        }
    } //fin del for

    $('[data-toggle="tooltip"]').tooltip();
    totalVentasCols();
    lenguaje();
}


let totalVentasCols = () => {
    document.getElementById('totalVentaCols').innerHTML = ``;
    let nFilas = $("#tablaBody > tr").length;
    let tablaC = document.getElementById("tablaBody"),
        rIndex;
    let columna = 7;
    let valorTotal = 0;
    let valor = 0;
    for (let i = 0; i < nFilas; i++) {
        //valorTotal +=  parseInt(convertirNumeros(document.getElementById('prect'+(i+1)).value));
        //console.log("valor total: " + valorTotal);
        valor += parseInt(convertirNumeros(tablaC.rows[i].cells[columna].innerHTML));

    }


    document.getElementById('totalVentaCols').innerHTML = `<h5>TOTAL: $${formatearNumeros(valor)}</h5>`;


}

function lenguaje() {


    var f = new Date();
    var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

    var table = $('#tablaProductos').DataTable({

        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "order": [
            [1, "asc"]
        ],
        "stateSave": true,
        "lengthMenu": [50, 100, 150, 175, 1000],

    });


    new $.fn.dataTable.Buttons(table, {
        buttons: [{
            extend: 'excelHtml5',
            title: 'ver_ventas' + fecha + ''
        }, {
            extend: 'pdfHtml5',
            title: 'ver_ventas' + fecha + ''
        }]

    });

    table.buttons(0, null).container().prependTo(
        table.table().container()
    );
}

function format(d) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Full name:</td>' +
        '<td>' + d.name + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Extension number:</td>' +
        '<td>' + d.extn + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Extra info:</td>' +
        '<td>And any further details here (images etc)...</td>' +
        '</tr>' +
        '</table>';
}


function eliminarProducto(e, id) {
    e.preventDefault();
    swal({
            title: "Eliminar producto",
            text: "¿esta seguro de eliminar el producto ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                //borrarVenta(id);
                obtenerStock(id);
            } else {
                return;
            }

        });

}

let obtenerStock = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `SELECT vr.id,vr.codigo_producto,p.id as idProducto,p.codigo_proveedor,id_cliente,p.precio_venta,vr.nombre_producto AS nombre,DATE(v.fecha_venta) AS fecha_venta, vr.cantidad,vr.precio_unitario,vr.total_unitario,vr.id_venta
	FROM ventas_relacional vr INNER JOIN ventas v ON v.id=vr.id_venta JOIN productos p ON p.codigo=vr.codigo_producto WHERE vr.id_venta=${idP}`;

    const sql = { sql: consulta, tag: `array_datos` }



    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.	

        let array = JSON.parse(data);


        for (let i = 0; i < array.length; i++) {

            const devol = await devolverStock(array[i]['cantidad'], array[i]['idProducto']);

        }

        const borrar = await borrarVenta(idP);


    } catch (error) { console.log('error en la conexion ', error); }

}


let devolverStock = async(cantidad, idProducto) => {

    const baseUrl = 'php/consultaFetch.php';

    const consulta = `UPDATE productos set stock =stock + (${cantidad}) WHERE id=${idProducto}`;

    const sql = { sql: consulta, tag: `array_datos` }



    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        console.error('actulizado');


    } catch (error) { console.log('error en la conexion ', error); }


}

let borrarVenta = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';

    let consulta = `DELETE FROM VENTAS  WHERE id=${idP}`;

    const sql = { sql: consulta, tag: `crud` }



    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();


        const borraVrelacional = borrVentaRe(idP);

    } catch (error) { console.log('error en la conexion ', error); }

}

let borrVentaRe = async(idP) => {

    const baseUrl = 'php/consultaFetch.php';
    let consulta = `DELETE FROM VENTAS_RELACIONAL  WHERE id_venta=${idP}`;

    const sql = { sql: consulta, tag: `crud` }



    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.	


        $.notify({
            title: "Borrado: ",
            message: "Se Borro la venta:",
            icon: 'fas fa-check'
        }, {
            type: "success",
            placement: {
                from: "top",
                align: "right"
            },
            offset: 70,
            spacing: 70,
            z_index: 1031,
            delay: 2000,
            timer: 3000
        });

        setTimeout('location.reload()', 1000);


    } catch (error) { console.log('error en la conexion ', error); }


}

let ventasDetalles = async() => {
    let numero = 0;

    let lanzamiento = setInterval(() => {
        document.getElementById('timer').innerHTML = `${numero} seg`;
        console.log(`${numero} seg`)
        numero++;
    }, 1000);
    document.getElementById('loading').innerHTML = `<strong><i class="fas fa-sync fa-spin fa-3x"></i>&nbsp;&nbsp;</strong>`;
    document.getElementById('contenido').className = `fade`;

    let fecha_inicio = document.getElementById('fecha_inicio').value;
    let fecha_termino = document.getElementById('fecha_termino').value;

    const baseUrl = 'php/consultaFetch.php';

    const consulta = `SELECT vr.id_venta,estado_venta,fecha_venta,p.codigo_proveedor,v.descuento,estado_venta,DATE(fecha_venta) AS fecha ,total,id_boleta,
    id_cotizacion,id_factura,id_guia,id_tarjeta,vr.cantidad,vr.nombre_producto,vr.codigo_producto,vr.precio_unitario,p.stock FROM
    ventas v INNER JOIN ventas_relacional vr ON vr.id_venta=v.id JOIN productos p ON vr.codigo_producto=p.codigo
    WHERE fecha_venta BETWEEN '${fecha_inicio} 00:00:00' AND '${fecha_inicio} 23:59:59' ORDER BY estado_venta,id_boleta,id_factura,id_guia,id_tarjeta ASC`;

    const sql = { sql: consulta, tag: `array_datos` }


    try {
        //*-llamar ajax al servidor mediate api fetch.
        const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
        //*-request de los datos en formato texto(viene todo el request)
        const data = await response.text();
        //*-se parsea solo la respuesta del Json enviada por el servidor.
        let codigo_producto;
        let fecha_venta
        let array = JSON.parse(data);
        clearInterval(lanzamiento);
        console.error('actulizado');

        console.log(array);
        document.getElementById('loading').innerHTML = ``;
        document.getElementById('contenido').className = `fade-in`;
        const ver = await tablaVerDetalles(array);

    } catch (error) { console.log('error en la conexion ', error); }

}

let tablaVerDetalles = async(arreglo) => {



    document.getElementById(`btn-exportar`).disabled = false;


    $("#salidaDetalle").html('');
    $("#salidaDetalle").append(`<table class="table table-striped " id="tablaProductosDetalle">
    <thead>
    <tr>
    <th width="10%">Cod Producto</th>
    <th width="10%">Cod Proveedor</th>
    <th width="10%">Tipo Venta</th>
    <th id="folio" width="10%">Folio</th>
    <th width="10%">Nombre</th>
    <th width="10%">Descuento</th>	
    <th width="10%">Cantidad</th>    
    <th width="10%">Precio Unitario</th>
    <th width="10%">Total Producto</th>										
    <th width="10%">Total Venta</th>					
    <th width="5%">Stock</th>
    <th width="5%">Fecha venta</th>
    </tr>
    </thead>
    <tbody id="tablaBodyDetalle"></tbody>
>
    </table>`);

    for (let i = 0; i < arreglo.length; i++) {

        let estadoDocumento;
        let numero;
        let resultado;
        resultado = parseInt(arreglo[i]['precio_unitario']) * parseInt(arreglo[i]['cantidad']);
        console.log(resultado);
        if (arreglo[i]['estado_venta'] == 1) {
            estadoDocumento = `Boleta`;
            numero = arreglo[i]['id_boleta'];


        } else if (arreglo[i]['estado_venta'] == 2) {

            estadoDocumento = `Factura`;
            numero = arreglo[i]['id_factura'];


        } else if (arreglo[i]['estado_venta'] == 3) {

            estadoDocumento = `Guia`;
            numero = arreglo[i]['id_guia'];


        } else if (arreglo[i]['estado_venta'] == 5) {

            estadoDocumento = `Tarjeta`;
            numero = arreglo[i]['id_tarjeta'];

        }

        if (arreglo[i]['estado_venta'] != 4) {

            $("#tablaBodyDetalle").append(`<tr>
					<td>${arreglo[i]['codigo_producto']}</td>			   
					<td>${arreglo[i]['codigo_proveedor']}</td>
					<td id="tipo_venta">${estadoDocumento}</td>
					<td id="folio">${numero}</td>
					<td>${arreglo[i]['nombre_producto']}</td>
					<td>${arreglo[i]['descuento']}</td>
                    <td>${arreglo[i]['cantidad']}</td>		
                    <td>${arreglo[i]['precio_unitario']}</td>	
                    <td>${resultado}</td>	
                    <td>${arreglo[i]['total']}</td>	  
					<td>${arreglo[i]['stock']}</td>	
                    <td>${arreglo[i]['fecha_venta']}</td>	  
			 		</tr>`);

        } else {
            console.error('4');
        }
    }


}

window.onload = vendedor