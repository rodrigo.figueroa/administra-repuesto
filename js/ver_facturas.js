var PROVEEDORES;



let proveedores = async () => {
	document.getElementById('loading').innerHTML=`<i class="fas fa-spinner fa-pulse"></i> Cargando facturas espere......`;
	const baseUrl = 'php/consultaFetch.php';
    let consulta=`	SELECT id,nombre FROM proveedores`;
	 
	
	const sql = {sql: consulta, tag: `array_datos`} 
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);
		var arr = new Array();
		for (var i = 0; i < array.length; i++) {
			arr[array[i][0].toString()] = array[i][1];

			}		
		PROVEEDORES=arr;
		const prove = await proveedorSelect();	
		const cargarFAct = await cargarFacturas();
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	

		
	} catch (error) {
		console.log('error en la conexion ', error);
	}	
}

let changueOnTurn = ()=>{
	$("#tablaBody").html("").fadeIn('slow');
	document.getElementById(`totalFacCols`).innerHTML='0';
	ACTIVO=false;
	if(ACTIVO==true){
	lenguaje();
	return;
	}
	// var fecha_inicio = $("#fecha_inicio").val();
	// var fecha_termino = $("#fecha_termino").val();
	cargarFacturas();

}


//select para filtro de vendedor
let proveedorSelect = async () => {

	//document.getElementById('loading').innerHTML=`<i class="fas fa-spinner fa-pulse fa-2x"></i> Cargando ventas, espere .....`;
	//document.getElementById('contenido').className=`fade`;

	var sql = 'SELECT id,nombre FROM proveedores  ORDER BY id';

	

	$.ajax({
		type: 'POST',
		url:  'php/consulta.php', 
		data: {sql: sql,tag: 'proveedorAsignado'},
		success:function (data){

					 $('#selectTurno').html(data).fadeIn();
					//  $('#selectCat option[value="2"]').attr("selected", true);		
					//consultarVendedor(fecha_actual,fecha_termino)
					
		},
		error: function (request, status, error)
					{alert('Error: Could not categoria');
		}
		})

  }

//*-cargar datos mediante async wait()
let cargarFacturas = async () => { 

	$("#salida").html('');

	document.getElementById('loading').innerHTML=`<i class="fas fa-spinner fa-pulse"></i> Cargando ventas espere......`;


	let selectVen=document.getElementById('selectTurno').value;
	var consulta;
	
	var filtroVendedor = 'AND id_proveedor=' + selectVen;

	// let tbody = document.getElementById('tablaBody');

	// tbody.innerHTML=``;

	let fecha_inicio=document.getElementById('fecha_inicio').value;				
	let fecha_termino=document.getElementById('fecha_termino').value;

	const baseUrl = 'php/consultaFetch.php';

	if (selectVen < 1) {

    consulta=`SELECT f.id,id_proveedor,p.rut,numero_factura,fecha_ingreso,neto,iva,total 
    FROM facturas f inner join proveedores p on p.id=f.id_proveedor WHERE fecha_ingreso between "${fecha_inicio} 00:00:00" AND "${fecha_termino} 23:59:59"
	ORDER BY f.id ASC`;

	}else{

		consulta=`SELECT f.id,id_proveedor,p.rut,numero_factura,fecha_ingreso,neto,iva,total 
		FROM facturas f inner join proveedores p on p.id=f.id_proveedor WHERE fecha_ingreso between "${fecha_inicio} 00:00:00" AND "${fecha_termino} 23:59:59"  ${filtroVendedor}
		ORDER BY f.id ASC`;	

	}
	
	const sql = {sql: consulta, tag: `array_datos`} 

	console.log(consulta);

	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);		
     
		const tablaFactutass = await tablaFacturas(array);
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	
		const botones = await lenguaje();	
		document.getElementById('loading').innerHTML=``;
	} catch (error) {
		console.log('error en la conexion ', error);
	}
	
}

 let tablaFacturas =  (arreglo) => {

	const salida=document.getElementById('salida');

	salida.innerHTML =`<table class="table table-striped " id="tablaProductos">
	<thead>
	<tr>
	<th width="15%">Proveedor</th>
	<th width="10%">Rut</th>
	<th width="10%">Factura</th>	
	<th width="10%">Fecha ingreso</th>
	<th width="5%">Neto</th>
	<th width="5%">Iva</th>
	<th width="5%">Total</th>							
	<th width="5%"> </th>
	<th width="5%"> </th>							
	</tr>
	</thead>
	<tbody id="tablaBody"></tbody>
	<tfoot>								
	<tr>
		<th id="totalFacCols" style="text-align:left">Total Facturas:</th>	
		<th></th>
		<th></th>
		<th></th>									
	</tr>
	</tfoot>	
	</table>`;


	for (var i = 0; i < arreglo.length; i++) {

		$("#tablaBody").append(`<tr>		   
		<td>${PROVEEDORES[arreglo[i]['id_proveedor']]}</td>
		   <td>${arreglo[i]['rut']}</td>
		   <td>${arreglo[i]['numero_factura']}</td>
		   <td>${arreglo[i]['fecha_ingreso']}</td>
		   <td>${formatearNumeros(arreglo[i]['neto'])}</td>
		   <td>${formatearNumeros(arreglo[i]['iva'])}</td>		
		   <td>${formatearNumeros(arreglo[i]['total'])}</td>				  
		   <td><form method="POST" action="editar_facturas.php">
		   <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
			data-placement="top" title="Editar" name="id" value=${arreglo[i]['id']}><i class="fas fa-pen" aria-hidden="true"></i></button></form></td>		
			<td ><button class="btn  btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${arreglo[i]['id']})><i class="fas fa-trash" aria-hidden="true"></i></button></td>			
		 </tr>`);
	}

	$('[data-toggle="tooltip"]').tooltip();

	totalFacturasCols();
 }


 let totalFacturasCols =async() => {

	let nFilas = $("#tablaBody > tr").length;
	let tablaC = document.getElementById("tablaBody"),
		rIndex;
	let columna=6;
	let valorTotal=0;
	let valor=0;
	for (let i = 0; i < nFilas; i++) {
		//valorTotal +=  parseInt(convertirNumeros(document.getElementById('prect'+(i+1)).value));
		//console.log("valor total: " + valorTotal);
		valor += parseInt(convertirNumeros(tablaC.rows[i].cells[columna].innerHTML));		
	  }
	  document.getElementById('totalFacCols').innerHTML=`<h5>Total facturas: $${formatearNumeros(valor)}</h5>`;
	  

}


function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaProductos').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "asc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}
function eliminarProducto(e, id,index) {
	e.preventDefault();
	let mensaje;
	let titulo;
	if(index==1){
		titulo=`Eliminar producto`;
		mensaje=`¿esta seguro de eliminar la boleta ?`;
	}else{
		titulo=`Anular producto`;
		mensaje=`¿esta seguro de anular la boleta ?`;
	}

	swal({
		title: `${titulo}`,
		text: `${mensaje}`,
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			elimiarVenta(id);
		
		} else {
			return;
		}

	});	
}

let elimiarVenta =async(idP)=>{

	const baseUrl = 'php/consultaFetch.php';

	let consulta=`DELETE FROM facturas WHERE id=${idP}`;

	const sql   = {sql: consulta, tag: `crud`}	
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();		
		const borraVrelacional = borrVentaRe (idP);				
	} catch (error) { console.log('error en la conexion ', error); }

}
let borrVentaRe = async (idP) => {

	const baseUrl = 'php/consultaFetch.php';
	let consulta=`DELETE FROM FACTURAS_RELACIONAL  WHERE idfactura=${idP}`;

	const sql   = {sql: consulta, tag: `crud`}	

	console.error(consulta);
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.	

			
			$.notify({
				title: "Borrado: ",
				message: "Se Borro la factura:",
				icon: 'fas fa-check'
			}, {
				type: "success",
				placement: {
					from: "top",
					align: "right"
				},
				offset: 70,
				spacing: 70,
				z_index: 1031,
				delay: 2000,
				timer: 3000
			});	

			setTimeout('location.reload()', 1000);
		
		
	} catch (error) { console.log('error en la conexion ', error); }


}

window.onload = proveedores