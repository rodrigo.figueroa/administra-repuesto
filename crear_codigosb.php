<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistema View Point</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
    <!-- Font-icon css-->
    
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">
<style>

.codigo {
    margin: -11em 5em ;
    font-weight: bold;
}
.codigo img{
    width: 30% !important;
   
}

</style>
</head>

<body class="app sidebar-mini rtl pace-done sidenav-toggled">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-store"></i> codigos </h1>
                <p>Ingresar codigos</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">codigos</li>
                <li class="breadcrumb-item active"><a href="#">Ingresar codigos</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                  <div class="float-right">
						<?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL,"es_ES");  echo "Fecha: ".strftime(" %d  %m  %Y"); ?>
            <br>
						<?php echo "Hora: ".date("H:i:s"); ?>
            <br> <br>
					</div><br>
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                    <div class="form-group col-md-3">
									<label class="negrita"> Codigo de barras </label>
									<div class="input-group">	
                                    <input type="number" class="form-control" id="codigob" name="codigob" min="0" placeholder="Ingrese valores numericos" value="0">									
									<div class="input-group-append">
                                    <button  target="_blank" class="btn btn-primary" id="addTipo_Equipo" onclick="crear_codigos()" data-toggle="tooltip" title="visualizar codigo" type="button"><i class="fas fa-barcode"></i> ver codigo</button>
                                </div>
                            </div>
                    </div>          
                    </div>
                    <!-- <div id='conTicket' class="codigo">

                    <!-- <div id="printBarcode"> <img id="barcode"/></div> -->
                    </div> 
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                               
							<br>
                        <!--Tabla ticket-->    
    <div id ="conTicket">
    <table id="tablaTicketCabezera">
    <tbody>
    <tr>
      <td id="logoTicket"></td>
      <tr>
      <tr>
      <td id="TituloTicketIngreso"></td>
      <tr>
      <tr>
      <td>
      </tr>
        <tr>
			<td id="Posticket" class="direccionIngresoClass" ></td>
		</tr>
		<tr>
			<td id="fechaTicket" class="VendedorTicketClass" ></td>
		</tr>
        </tbody>
    </table>
    <table id="tablaTicket">
    <thead>    
        <tr>
			<th id="colNombre"></th>
            <th id="colCantidad"></th>
            <th></th>
            <th id="colPrecio"></th>
            <tr>
        
            </tr>		
		</tr>
    </thead>
	<tbody id="Filaticket">	
	    	
    </tbody>
         <tr>
         <tr>  
         </tr>	
			<td id="totalCompra"></td>
            <td></td>
            <td></td>
            <td id="totalCompra_precio"></td>
            <td></td>
		</tr>
<tr>
        <td></td>
        <td></td>
        <td></td>
        <td id="c_pago"></td>
        <td></td>
        </tr>	
 <tr>
        <td></td>
        <td></td>
        <td></td>
        <td id="total_vuelto"></td>
        <td></td>
        </tr>		

       
</table>
<div id="printBarcode" class="codigo"> <img id="barcode"/></div>
         
                            <br>
                            <br>
                            <br>
                            <br>
                            
							</div>
                            <button class="btn btn-primary float-right" id="fina_venta" onkeypress="enterVenta(event,'conTicket')" onclick="prinCodbar(event,'conTicket')"><i class="fa fa-save"></i> Imprimir codigo </button>
                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div> 


<!-- The Modal -->

    </div>
  </div> 
</div>

    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/crear_codigosb.js?vp5"></script>  
    <script type="text/javascript" src="js/funciones.js?vp5"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
    <script type="text/javascript" src="js/plugins/JsBarcode.all.min.js"></script>

    <script>
        <?php  $fecha  = date("Y-m-d") ?>;
        <?php $hora =   date("H:i:s") ?>;
        var FECHA = '<?php echo date("d-m-Y",strtotime($fecha)); ?>';
        var HORA = '<?php echo $hora ?>';
        var ID_VENDEDOR =<?php echo $idVendedor;?>; 
        var ID_TURNO = <?php echo $idTurno;?>;   
        
</script>

</body>

</html>
